using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin_Control : MonoBehaviour
{

    public Transform bottombunObj;
    public Transform topbunObj;
    public Transform burgerObj;
    public Transform backrollObj;
    public Transform frontrollObj;
    public Transform hotdogObj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        if (gameObject.name == "bun bin")
        {
            if (Gameplay.cuttingboardS1 == "empty")
            {
                Instantiate(bottombunObj, new Vector2(-1, -.9f), bottombunObj.rotation);
                Instantiate(topbunObj, new Vector2(-1, -.5f), topbunObj.rotation);
                Gameplay.cuttingboardS1 = "full";
            }
        else
            if (Gameplay.cuttingboardS2 == "empty")
            {
                Instantiate(bottombunObj, new Vector2(-1, -.9f), bottombunObj.rotation);
                Instantiate(topbunObj, new Vector2(-1, -.5f), topbunObj.rotation);
                Gameplay.cuttingboardS2 = "full";
            }

        }
        else
            if (Gameplay.cuttingboardS3 == "empty")
        {
            Instantiate(bottombunObj, new Vector2(-1, -.9f), bottombunObj.rotation);
            Instantiate(topbunObj, new Vector2(-1, -.5f), topbunObj.rotation);
            Gameplay.cuttingboardS3 = "full";
            }

    }
}
}
